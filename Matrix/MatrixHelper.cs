﻿using System;
using System.Threading.Tasks;

namespace Matrix
{
    public static class MatrixHelper
    {
        public static Matrix MatricGeneration(int n, int start = 42)
        {
            var matrix = new Matrix(n);
            var rnd = new Random(start);
            var parallelOptions = new ParallelOptions { MaxDegreeOfParallelism = 6 };
            Parallel.For(0, n * n, parallelOptions, i => matrix.SetElementArray(i, Math.Round(rnd.Next(-100, 100) + rnd.NextDouble())));
            return matrix;
        }
    }
}
