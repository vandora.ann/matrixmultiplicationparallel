﻿using System;
using System.Threading.Tasks;

namespace Matrix
{
    public class Matrix
    {
        private double[] Data;

        private int n;
        public int N { get => this.n; }

        public Matrix(int n)
        {
            this.n = n;
            Data = new double[n * n];
        }

        public Matrix(int n, double[] data)
        {
            if (data.Length != n * n)
                throw new ArgumentException("Неверная размерность матрицы");

            this.n = n;
            Data = data;
        }

        public double[] GetData()
        {
            return Data;
        }

        public double GetElement(int i, int j)
        {
            return Data[i * n + j];
        }

        public double GetElementArray(int i)
        {
            return Data[i];
        }

        public void SetElement(int i, int j, double element)
        {
            Data[i * n + j] = element;
        }

        public void SetElementArray(int i, double element)
        {
            Data[i] = element;
        }

        public bool IsEquals(Matrix matrix)
        {
            if (N != matrix.N)
                return false;

            var parallelOptions = new ParallelOptions { MaxDegreeOfParallelism = 4 };
            var result = true;
            Parallel.For(0, n, parallelOptions, i =>
            {
                var line = i * n;
                Parallel.For(0, n, parallelOptions, async j =>
                {
                    if (Math.Abs(Data[line + j] - matrix.Data[line + j]) > 0.00001)
                        result = false;
                });
            });
            return result;
        }

        public static Matrix operator *(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1.N != matrix2.N)
                throw new ArgumentException("Матрицы разных размерностей");

            var n = matrix1.N;
            var result = new Matrix(n);

            for (var i = 0; i < n; ++i)
            {
                for (var j = 0; j < n; ++j)
                {
                    var element = 0.0;
                    for (var k = 0; k < n; ++k)
                    {
                        element += matrix1.GetElement(i, k) * matrix2.GetElement(k, j);
                    }
                    result.SetElement(i, j, element);
                }
            }

            return result;
        }

        public static async Task<Matrix> MultiplicationParallel(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1.N != matrix2.N)
                throw new ArgumentException("Матрицы разных размерностей");

            var n = matrix1.N;
            var result = new Matrix(n);
            var parallelOptions = new ParallelOptions { MaxDegreeOfParallelism = 4 };

            Parallel.For(0, n, parallelOptions, i =>
            {
                var line = i * n;
                Parallel.For(0, n, parallelOptions, async j =>
                 result.SetElementArray(line + j, await Element(matrix1.GetData(), matrix2.GetData(), n, line, j, 0, n - 1)));
            });

            return result;
        }

        private static async Task<double> Element(double[] x, double[] y, int n, int line, int j)
        {
            var r = 0.0;

            for (var k = 0; k < n; k++)
            {
                r += x[line + k] * y[k * n + j];
            }
            return r;
        }

        private static async Task<double> Element(double[] x, double[] y, int n, int line, int j, int start, int stop)
        {
            if (stop - start == 1)
                return x[line + start] * y[start * n + j] + x[line + stop] * y[stop * n + j];

            if (start == stop)
                return x[line + start] * y[start * n + j];

            var k = (start + stop) / 2;

            var leftSum = Element(x, y, n, line, j, start, k);
            var rightSum = Element(x, y, n, line, j, k + 1, stop);

            Task.WaitAll(leftSum, rightSum);
            return leftSum.Result + rightSum.Result;
        }
    }
}
