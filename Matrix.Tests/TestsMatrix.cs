using System.Threading.Tasks;
using NUnit.Framework;

namespace Matrix.Tests
{
    public class Tests
    {
        [Test]
        public void MultTest()
        {
            //// 1  2  0     2 -1  0      4  1 -4
            //// 2 -1 -3  *  1  1 -2  =  12 -9 -1   
            //// 0  2  1    -3  2  1     -1  4 -3
            var mat1 = new Matrix(3, new double[] { 1, 2, 0, 2, -1, -3, 0, 2, 1 });

            var mat2 = new Matrix(3, new double[] { 2, -1, 0, 1, 1, -2, -3, 2, 1 });

            var mat = new Matrix(3, new double[] { 4, 1, -4, 12, -9, -1, -1, 4, -3 });

            var mat3 = Matrix.MultiplicationParallel(mat1, mat2).Result;

            Assert.IsTrue(mat.IsEquals(mat3));
        }

        [Test]
        public void MultTest4x4()
        {
            //// 1   2  0   0.4     2 -1  0  2.1     4.4	2.2	  -4  2.46
            //// 2  -1 -3    -1  *  1  1 -2    2  =   11	-12	  -1  11.3   
            //// 0   2  1   1.5    -3  2  1    0    -0.5	8.5	  -3 -9.65
            ////-3 0.2 -0.1  -1     1  3  0 -9.1    -6.5	  0	-0.5   3.2
            var mat1 = new Matrix(4, new double[] { 1, 2, 0, 0.4, 2, -1, -3, -1, 0, 2, 1, 1.5, -3, 0.2, -0.1, -1 });

            var mat2 = new Matrix(4, new double[] { 2, -1, 0, 2.1, 1, 1, -2, 2, -3, 2, 1, 0, 1, 3, 0, -9.1 });

            var mat = new Matrix(4, new double[] { 4.4, 2.2, -4, 2.46, 11, -12, -1, 11.3, 0.5, 8.5, -3, -9.65, -6.5, 0, -0.5, 3.2 });

            var matSimple = mat1 * mat2;
            Assert.IsTrue(mat.IsEquals(matSimple));

            var matParallel = Matrix.MultiplicationParallel(mat1, mat2).Result;
            Assert.IsTrue(mat.IsEquals(matParallel));
        }

        [Test]
        public async Task MultTestRand()
        {
            var n = 100;
            var mat1 = MatrixHelper.MatricGeneration(n);
            var mat2 = MatrixHelper.MatricGeneration(n, -42);

            var mat3 = Matrix.MultiplicationParallel(mat1, mat2).Result;
            var mat4 = mat1 * mat2;

            Assert.IsTrue(mat3.IsEquals(mat4));
        }
    }
}