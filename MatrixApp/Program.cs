﻿using System;
using System.Diagnostics;

namespace MatrixApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var stop = false;
            while (!stop)
            {
                Console.WriteLine();

                Console.WriteLine("Введите размерность матрицы:");
                var size = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("0 - ввести элементы матриц вручную;");
                Console.WriteLine("не 0 - заполнить матрицы случайными числами");
                var option = Convert.ToInt32(Console.ReadLine());

                Matrix.Matrix mat1;
                Matrix.Matrix mat2;

                if (option == 0)
                {
                    mat1 = InputElements(size);
                    mat2 = InputElements(size);
                }
                else
                {
                    mat1 = Matrix.MatrixHelper.MatricGeneration(size);
                    mat2 = Matrix.MatrixHelper.MatricGeneration(size, -42);
                }

                Console.WriteLine();
                Console.WriteLine("----------Умножение матриц----------");
                var sw = Stopwatch.StartNew();
                sw.Start();
                var result = Matrix.Multiplication.MultiplicationParallel(mat1, mat2).Result;
                sw.Stop();
                Console.WriteLine(
                    $"Длительность умножения: {sw.Elapsed.Minutes} мин. : {sw.Elapsed.Seconds} с. : {sw.Elapsed.Milliseconds} мс.");
                Console.WriteLine("Распечатать? Y");
                var answer = Console.ReadLine();

                if (answer == "Y" || answer == "y")
                {
                    for (var i = 0; i < size; i++)
                    {
                        for (var j = 0; j < size; j++)
                        {
                            Console.Write(Math.Round(result.GetElement(i, j), 2));
                            Console.Write(" ");
                        }
                        Console.WriteLine();
                    }
                }

                Console.WriteLine("Завершить работу? Y");
                answer = Console.ReadLine();
                stop = answer == "Y" || answer == "y";
            }
        }

        private static Matrix.Matrix InputElements(int size)
        {
            var mat = new Matrix.Matrix(size);
            for (var i = 0; i < size; i++)
            {
                for (var j = 0; j < size; j++)
                {
                    Console.WriteLine($"Элемент ({i}, {j}):");
                    mat.SetElement(i, j, Convert.ToDouble(Console.ReadLine()));
                }
            }

            return mat;
        }
    }
}
